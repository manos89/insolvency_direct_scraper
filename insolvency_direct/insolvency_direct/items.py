# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst


class InsolvencyDirectItem(scrapy.Item):
    # define the fields for your item here like:
    Surname = scrapy.Field(output_processor=TakeFirst())
    Forename = scrapy.Field(output_processor=TakeFirst())
    Date_of_birth = scrapy.Field(output_processor=TakeFirst())
    Last_known_address = scrapy.Field(output_processor=TakeFirst())
    url = scrapy.Field(output_processor=TakeFirst())
    Arrangement_date = scrapy.Field(output_processor=TakeFirst())
    Status = scrapy.Field(output_processor=TakeFirst())
    Main_insolvency = scrapy.Field(output_processor=TakeFirst())
    Firm_Name = scrapy.Field(output_processor=TakeFirst())

    def set_to_none(self, cls):
        # print(cls.fields)
        for field in cls.fields:
            cls[field] = None
