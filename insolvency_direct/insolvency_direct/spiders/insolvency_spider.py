import scrapy
import string
from insolvency_direct.items import InsolvencyDirectItem

class InsolvencySpider(scrapy.Spider):
    name = "insolvency"
    url = "https://www.insolvencydirect.bis.gov.uk/eiir/IIRSearchNames.asp?court=ALL&CourtName=&Office=&OfficeName=&page={0}&surnamesearch={1}&forenamesearch=ALLFORENAMES&OPTION=NAME&tradingnamesearch="

    def start_requests(self):
        for letter in string.ascii_uppercase:
            req = scrapy.Request(self.url.format("1", letter), callback=self.parse)
            req.meta["letter"] = letter
            req.meta["page"] = 1
            yield req

    def parse(self, response):
        letter = response.meta["letter"]
        page = response.meta["page"]
        rows = response.selector.css("table.DataTable > tbody > tr")
        for row in rows:
            row_type = row.css("td::text").extract()[-1]
            if row_type.strip() == "IVA":
                link = "https://www.insolvencydirect.bis.gov.uk/eiir/" + row.css("a::attr(href)").extract_first().strip()
                req = scrapy.Request(link, callback=self.parse_details, dont_filter=True)
                # req = scrapy.Request("https://www.insolvencydirect.bis.gov.uk/eiir/IIRCaseIndivDetail.asp?"
                #                      "CaseId=703042963&IndivNo=706974555&Court=VOL&OfficeID=600000127&CaseType=I",
                #                      callback=self.parse_details)
                yield req
        all_links = [l.strip() for l in response.selector.css("a::text").extract()]
        if "Last>>" in all_links:
            next_page = page + 1
            req = scrapy.Request(self.url.format(str(next_page), letter), callback=self.parse)
            req.meta["letter"] = letter
            req.meta["page"] = next_page
            yield req

    def parse_details(self, response):
        rows = response.selector.css("tr")
        itm = InsolvencyDirectItem()
        itm.set_to_none(itm)
        itm["url"] = response.request.url
        for row in rows:
            # field = row.css("td::text").extract_first()
            # print(field)
            try:
                field = row.css("td > strong::text").extract_first()
                value = row.css('td::text').extract()
                if field == "Surname":
                    itm["Surname"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Forename(s)":
                    itm["Forename"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Date of Birth":
                    itm["Date_of_birth"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Last Known Address":
                    itm["Last_known_address"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Arrangement Date":
                    itm["Arrangement_date"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Status":
                    itm["Status"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Main Insolvency Practitioner ":
                    itm["Main_insolvency"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
                if field == "Firm ":
                    itm["Firm_Name"] = " ".join(value).replace("\r", "").replace("\t", "").replace("\n", "").strip()
            except:
                pass
        yield itm